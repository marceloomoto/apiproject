package com.mytest.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Before;
import org.junit.Test;

import com.mytest.model.Login;

/**
 * API Login Tests
 * 
 * @author omoto
 *
 */
public class LoginTest {

    private Login login;

    /**
     * Test Pre Conditions
     */
    @Before
    public void prepareTest() {

        baseURI = "http://reqres.in";

    }

    /**
     * Tests Login Success
     */
    @Test
    public void testLoginSuccess() {

        login = new Login("peter@klaven", "cityslicka");

        given().contentType("application/json")
                .body(login)
                .when()
                .post("/api/login")
                .then()
                .statusCode(200)
                .body("token", equalTo("QpwL5tke4Pnpja7X"));

    }

    /**
     * Tests Error - Missing Password
     */
    @Test
    public void testErrorPasswordMissing() {

        login = new Login("peter@klaven");

        given().contentType("application/json")
                .body(login)
                .when()
                .post("/api/login")
                .then()
                .statusCode(400)
                .body("error", equalTo("Missing password"));

    }

}
